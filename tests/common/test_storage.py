import pytest
import json
import os.path
import unittest.mock as mock
import sjb.common.config
from sjb.common.storage import BaseStorage

_APP = 'test-app'
_LIST = 'test-list'

class TestStorage(object):

  def mock_factory(self):
    return mock.Mock(methods=['from_dict'])

  def test_get_list_name_default(self):
    s = BaseStorage(self.mock_factory(), _APP, _LIST)
    assert s.get_list_name() == 'test-list'

  @mock.patch('sjb.common.config.get_user_app_data_dir', autospec=True, return_value='user/apps/data/app')
  def test_get_list_file_default(self, mock_get_data_dir):
    s = BaseStorage(self.mock_factory(), _APP, _LIST)
    assert s.get_list_file() == 'user/apps/data/app/test-list.json'

    mock_get_data_dir.assert_called_once_with('test-app', suite_name='sjb')

  @mock.patch('os.path.isdir', return_value=True, autospec=True)
  @mock.patch('os.makedirs')
  @mock.patch('sjb.common.misc.backup_file')
  @mock.patch('builtins.open', return_value=mock.Mock(methods=['write', 'close']))
  @mock.patch('json.dumps', return_value='output json')
  def test_save_file_parent_exists(self, mock_json, mock_open, mock_backup, mock_mkdirs, mock_isdir):
    s = BaseStorage(self.mock_factory(), _APP, _LIST)
    s.get_list_file = mock.Mock()
    s.get_list_file.return_value = '/some/dir/file.json'
    mock_list = mock.Mock(methods=['validate', 'to_dict'])
    mock_list.to_dict.return_value = {'fake': 'dict'}

    s.save_list(mock_list)

    s.get_list_file.assert_called_once_with()
    mock_isdir.assert_called_once_with('/some/dir')
    mock_mkdirs.assert_not_called()
    mock_backup.assert_called_once_with('/some/dir/file.json', '.backup')
    mock_list.validate.assert_called_once_with()
    mock_json.assert_called_once_with({'fake': 'dict'}, indent=2)
    mock_open.assert_called_once_with('/some/dir/file.json', 'w')
    mock_open.return_value.write.assert_called_once_with('output json')
    mock_open.return_value.close.assert_called_once_with()
    mock_list.to_dict.assert_called_once_with()

  @mock.patch('os.path.isdir', return_value=False, autospec=True)
  @mock.patch('os.makedirs')
  @mock.patch('sjb.common.misc.backup_file')
  @mock.patch('builtins.open', return_value=mock.Mock(methods=['write', 'close']))
  @mock.patch('json.dumps', return_value='output json')
  def test_save_file_parent_notexists(self, mock_json, mock_open, mock_backup, mock_mkdirs, mock_isdir):
    s = BaseStorage(self.mock_factory(), _APP, _LIST)
    s.get_list_file = mock.Mock()
    s.get_list_file.return_value = '/some/dir/file.json'
    mock_list = mock.Mock(methods=['validate', 'to_dict'])
    mock_list.to_dict.return_value = {'fake': 'dict'}

    s.save_list(mock_list)

    s.get_list_file.assert_called_once_with()
    mock_isdir.assert_called_once_with('/some/dir')
    mock_mkdirs.assert_called_once_with('/some/dir')
    mock_backup.assert_called_once_with('/some/dir/file.json', '.backup')
    mock_list.validate.assert_called_once_with()
    mock_json.assert_called_once_with({'fake': 'dict'}, indent=2)
    mock_open.assert_called_once_with('/some/dir/file.json', 'w')
    mock_open.return_value.write.assert_called_once_with('output json')
    mock_open.return_value.close.assert_called_once_with()
    mock_list.to_dict.assert_called_once_with()

  @mock.patch('os.path.isfile', return_value=True)
  @mock.patch('os.path.exists', return_value=False)
  @mock.patch('builtins.open', return_value=mock.Mock(methods=['close']))
  @mock.patch('json.load', return_value={'fake': 'dict'})
  def test_load_file(self, mock_load, mock_open, mock_exists, mock_isfile):
    mock_factory = self.mock_factory()
    s = BaseStorage(mock_factory, _APP, _LIST)
    s.get_list_file = mock.Mock()
    s.get_list_file.return_value = '/some/dir/file.json'
    mock_factory.return_value = mock.Mock(methods=['validate'])

    lst = s.load_list()
    assert lst == mock_factory.from_dict.return_value

    s.get_list_file.assert_called_once_with()
    mock_isfile.assert_called_once_with('/some/dir/file.json')
    mock_load.assert_called_once_with(mock_open.return_value)
    mock_open.return_value.close.assert_called_once_with()
    mock_factory.from_dict.assert_called_once_with({'fake': 'dict'})
    mock_factory.from_dict.return_value.validate.assert_called_once_with()

  @mock.patch('os.path.isfile', return_value=False)
  @mock.patch('os.path.exists', return_value=False)
  def test_load_file_wrong_filetype(self, mock_exists, mock_isfile):
    s = BaseStorage(self.mock_factory(), _APP, _LIST)
    s.get_list_file = mock.Mock()
    s.get_list_file.return_value = '/some/dir/file.json'

    with pytest.raises(sjb.common.storage.NoListFileError):
      s.load_list()

  @mock.patch('os.path.isfile', return_value=False)
  @mock.patch('os.path.exists', return_value=True)
  def test_load_file_not_exists(self, mock_exists, mock_isfile):
    s = BaseStorage(self.mock_factory(), _APP, _LIST)
    s.get_list_file = mock.Mock()
    s.get_list_file.return_value = '/some/dir/file.json'

    with pytest.raises(sjb.common.storage.IOError):
      s.load_list()
