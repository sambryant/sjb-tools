"""Module responsible for reading/writing item list json to file."""
import os
import json
import sjb.common.config

_SUITE = 'sjb'
_LIST_FILE_EXTENSION = '.json'
_BACKUP_EXTENSION = '.backup'


class NoListFileError(Exception):
  """Raised when user tries to load a non-existent list."""
  pass

class IOError(Exception):
  """Raised on generic problem with writing things to/from OS."""
  pass

class BaseStorage(object):
  """Class encapsulating environment information like where to write stuff."""

  def __init__(self, factory, appname, listname):
    self._listname = listname
    self._appname = appname
    self._factory = factory

  def get_list_file(self):
    return os.path.join(
      sjb.common.config.get_user_app_data_dir(
        self._appname, suite_name=_SUITE),
        '%s%s' % (self._listname, _LIST_FILE_EXTENSION))

  def get_list_name(self):
    """Returns the short name of the list for this storage object."""
    return self._listname

  def get_all_list_files(self):
    """Returns a list of all the available list files in the data directory."""
    d = sjb.common.config.get_user_app_data_dir(
        self._appname, suite_name=_SUITE)
    files = os.listdir(d)
    matching = []
    for f in files:
      if not os.path.isfile(os.path.join(d, f)):
        continue
      # check that it has correct extension.
      if not f.endswith(_LIST_FILE_EXTENSION):
        continue
      matching.append(f[0:(len(f)-len(_LIST_FILE_EXTENSION))])
    return matching

  def save_list(self, item_list):
    """Saves the list to the file pointed at by this object.

    :param item_list: The list object to save to file.
    :type item_list: :class:`sjb.common.base`

    Raises:
      sjb.td.classes.ValidationError: If some element of the list is invalid.
    """
    fname = self.get_list_file()

    # create parent directory as needed
    if not os.path.isdir(os.path.dirname(fname)):
      os.makedirs(os.path.dirname(fname))

    sjb.common.misc.backup_file(fname, _BACKUP_EXTENSION)

    item_list.validate()

    json_file = open(fname, 'w')
    json_file.write(json.dumps(item_list.to_dict(), indent=2))
    json_file.close()

  def load_list(self):
    """Loads the item list.

    The name of the item list is specified at initialization time.

    Raises:
      ValidationError: If some element of the list is invalid.
      NoListFileError: If the file does not exist.
      IOError: If a file-like object exists but is wrong type (i.e. a dir).
    """
    fname = self.get_list_file()
    if not os.path.isfile(fname):
      if os.path.exists(fname):
        raise IOError('list file exists but is of wrong filetype')
      raise NoListFileError()

    json_file = open(fname, 'r')
    json_dict = json.load(json_file)
    json_file.close()
    lst = self._factory.from_dict(json_dict)
    lst.validate()
    return lst
